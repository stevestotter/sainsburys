Readme
============

To run the PHP program from the console, please navigate to the project root directory and 
execute the command:

~~~
php ProductScraper.php
~~~

This will run the program on the provided link specified in the coding challenge document. 
Please note: You will need php to be included in the PATH of your system.

I have used Composer as a dependency manager to incorporate PHPUnit. Composer.phar is included
in the files just for my usage (I do not have Composer set up system-wide).

To run the tests I have written for this application, please run the following command from 
the project root:

~~~
vendor\bin\phpunit _tests
~~~