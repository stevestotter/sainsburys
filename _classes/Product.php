<?php

/**
 * Product object - A basic class to store groceries with their associated 
 * properties. Can be extended further in the future as the business requires. 
 */
class Product {
    
    public $title;
    public $size;
    public $unit_price;
    public $description;
    
    /**
     * Construct of the Product object
     * @param $title
     * @param $size
     * @param $unit_price
     * @param $description
     */
    function __construct($title, $size, $unit_price, $description) {
        $this->title = $title;
        $this->size = $size;
        $this->unit_price = $unit_price;
        $this->description = $description;
    }
    
    /**
    * Gets unit_price member from this Product object.
    * @return Double unit_price.
    */
    function getUnitPrice() {
        return $this->unit_price;
    }
}

