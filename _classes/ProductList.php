<?php

require_once '_classes/Product.php';

/**
 * ProductList object - A class to store grouped products, with a running cost
 * of the total list maintained. 
 */

class ProductList {
    
    public $results;
    public $total;
    
    /**
     * Construct of the ProductList object
     * - initialises new Product array $results and sets running total cost of 
     * the value of the list $total to 0.
     */
    function __construct() {
        $this->results = array();
        $this->total = 0;
    }
    
    /**
    * Adds a new Product and recalculates the running total cost. 
    * @param Product $product <p>
    * The Product object to add to this ProductList.
    * </p>
    */
    function addProduct(Product $product) {
        $this->results[] = $product;
        $this->total += $product->getUnitPrice();
        $this->total = number_format($this->total, 2);
    }
    
    /**
    * Outputs the ProductList object to JSON format. 
    */
    function toJSON() {
        return json_encode($this);
    }
}

