<?php

/**
 * A library class to provide helpful HTML static methods and processes for this
 * exercise.
 */

class HTMLProcessor {
    
    /**
    * Generates a DOMDocument from a provided HTML String
    * @param String $html <p>
    * Valid HTML that can be converted into a DOMDocument.
    * </p>
    * @return DOMDocument DOM of the parsed HTML.
    */
    static function HTMLStringToDOMObject($html) {
        // Stops warnings from loadHTML() method in DOMDocument class when parsing 
        libxml_use_internal_errors(true);

        $dom = new DOMDocument();
        $dom->loadHTML($html);
        return $dom;
    }
    
    /**
    * cURLs a specified address in order to return a HTML string of the resource.
    * @param String $address <p>
    * Address to be fetched via cURL.
    * </p>
    * @return String HTML of page to be returned.
    */
    static function fetchHTML($address) {
        $c = curl_init($address);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        
        $html = curl_exec($c);
        $httpCode = curl_getinfo($c, CURLINFO_HTTP_CODE);

        if (curl_error($c)) {
            throw new Exception(curl_error($c));
        } else if($httpCode == 404 || $httpCode == 403) {
            throw new Exception('Error retrieving page: ' . $httpCode);
        }
        
        curl_close($c);
        return $html;
    }
    
    /**
    * Returns a textContent from a singular object in a DOMDocument 
    * @param String $query <p>
    * The XPath query.
    * </p>
    * @param DOMXPath $xpath <p>
    * A DOMXPath object used to locate a given object.
    * </p>
    * @return String Trimmed version of the found object's textContent.
    */
    static function scrapeObjectText($query, DOMXPath $xpath) {
        $result = $xpath->query($query);
        if($result->item(0)) {
            $object = trim($result->item(0)->textContent);
            return $object;
        } else {
            throw new Exception("Error: Cannot retrieve expected item from DOM");
        }
    }
    
    /**
    * Returns the product title from the page from a singular object in a DOMDocument 
    * @param DOMXPath $xpath <p>
    * A DOMXPath object used to locate a given object.
    * </p>
    * @return String Trimmed version of the found object title textContent.
    */
    static function getProductTitleFromDOM(DOMXPath $xpath) {
        return HTMLProcessor::scrapeObjectText("//*[@class='productTitleDescriptionContainer']/h1", $xpath);
    }

    /**
    * Returns the product price from the page from a singular object in a DOMDocument 
    * @param DOMXPath $xpath <p>
    * A DOMXPath object used to locate a given object.
    * </p>
    * @return String Trimmed and formatted version of the found object price.
    */
    static function getProductPriceFromDOM(DOMXPath $xpath) {
        $productPrice = HTMLProcessor::scrapeObjectText("//*[@class='pricePerUnit']", $xpath);
        return number_format(preg_replace("/[^0-9\.]/", "", $productPrice), 2);
    }

    /**
    * Returns the product description from the page from a singular object in a DOMDocument 
    * @param DOMXPath $xpath <p>
    * A DOMXPath object used to locate a given object.
    * </p>
    * @return String Trimmed version of the found object description textContent.
    */
    static function getProductDescriptionFromDOM(DOMXPath $xpath) {
        // Find the 'Description' header and then grab its sibling
        return HTMLProcessor::scrapeObjectText("//*[@class='productDataItemHeader'][text()='Description']/following-sibling::*[@class='productText']", $xpath);
    }
}

