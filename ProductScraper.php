<?php

/*
 * Entry point for application.
 */

require_once '_classes/HTMLProcessor.php';
require_once '_classes/ProductList.php';
require_once '_classes/Product.php';

try {
    // Fetch page and get DOM object
    $html = HTMLProcessor::fetchHTML('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html');
    $dom = HTMLProcessor::HTMLStringToDOMObject($html);
    $xpath = new DOMXPath($dom);

    // Narrow down the DOM page object to just the products
    $productsNodeList = $xpath->query("//*[@class='productInfo']");

    $productList = new ProductList();

    // Loop through products on the page
    foreach($productsNodeList as $productNode) {
        // Get product page link
        $productLink = $xpath->query("h3/a", $productNode);
        if(is_null($productLink->item(0))) {
            throw new Exception("Error in retrieving projects on page");
        }
        $href = $productLink->item(0);

        $url = $href->getAttribute('href');
        if(empty($url)) {
            throw new Exception("Error in retrieving product information page");
        }

        $productPageHtml = HTMLProcessor::fetchHTML($url);
        $productPageDom = HTMLProcessor::HTMLStringToDOMObject($productPageHtml);
        $productPageXpath = new DOMXPath($productPageDom);

        // strlen returns the number of bytes in the html response string
        $productPageSize = round(strlen($productPageHtml) / 1024, 1) . 'kb'; 
        $productTitle = HTMLProcessor::getProductTitleFromDOM($productPageXpath);
        $productDescription = HTMLProcessor::getProductDescriptionFromDOM($productPageXpath);
        $productPrice = HTMLProcessor::getProductPriceFromDOM($productPageXpath);        

        $product = new Product($productTitle, $productPageSize, $productPrice, $productDescription);
        $productList->addProduct($product);   
    }

    // Output final JSON object of all the parsed products on the page
    echo $productList->toJson();

} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}

