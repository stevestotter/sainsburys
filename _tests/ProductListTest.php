<?php

require_once "vendor/autoload.php";
require_once "_classes/ProductList.php";

class ProductListTest extends PHPUnit_Framework_TestCase
{
    // Testing adding a product to a ProductList
    public function testAddProduct() {
        $productList = new ProductList();

        $mockProduct = $this->getMockBuilder('Product')
            ->disableOriginalConstructor()
            ->getMock();
        
        $mockProduct->expects($this->once())->method('getUnitPrice')->will($this->returnValue(1.80));
        
        $expected = 1.80;
        $productList->addProduct($mockProduct);
        $actual = $productList->total;
        $this->assertEquals($expected, $actual);
    }
    
    // Testing cost total adding two products to a ProductList
    public function testAddTwoProducts() {
        $productList = new ProductList();

        $mockProduct = $this->getMockBuilder('Product')
            ->disableOriginalConstructor()
            ->getMock();
        
        $mockProduct->expects($this->once())->method('getUnitPrice')->will($this->returnValue(2.60));
        
        $mockProduct2 = $this->getMockBuilder('Product')
            ->disableOriginalConstructor()
            ->getMock();
        
        $mockProduct2->expects($this->once())->method('getUnitPrice')->will($this->returnValue(1.70));
        
        $productList->addProduct($mockProduct);
        $productList->addProduct($mockProduct2);
        
        $expected = 4.30;
        $actual = $productList->total;
        $this->assertEquals($expected, $actual);
    }
}

