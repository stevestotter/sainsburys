<?php

require_once "vendor/autoload.php";
require_once "_classes/HTMLProcessor.php";

class HTMLProcessorTest extends PHPUnit_Framework_TestCase
{
    protected $domDocument;
    protected $xpath;
    
    protected function setUp(){
        // Stops warnings from loadHTML() method in DOMDocument class when parsing 
        libxml_use_internal_errors(true);

        $this->domDocument = new DOMDocument();
        $this->domDocument->load('_resources/testXML.xml');
        $this->xpath = new DOMXPath($this->domDocument);
    }
    
    function tearDown(){}
    
    function testGetProductTitleFromDOM() {
        $actual = HTMLProcessor::getProductTitleFromDOM($this->xpath);
        $expected = "Sainsbury's Kiwi Fruit, Ripe & Ready x4";
        $this->assertEquals($expected, $actual);
    }
    
    function testGetProductPriceFromDOM() {
        $actual = HTMLProcessor::getProductPriceFromDOM($this->xpath);
        $expected = 1.80;
        $this->assertEquals($expected, $actual);
    }
    
    function testGetProductDescriptionFromDOM() {
        $actual = HTMLProcessor::getProductDescriptionFromDOM($this->xpath);
        $expected = "Kiwi";
        $this->assertEquals($expected, $actual);
    }
    
    function testScrapeTextObjectNotFound() {
        $this->setExpectedException('Exception', 'Error: Cannot retrieve expected item from DOM');
        HTMLProcessor::scrapeObjectText("//*[@class='productTitleDescriptionContainerFail']/h1", $this->xpath);
    }
    
    function testFetchHTML403() {
        $address = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrapeerror/5_products.html';
        $this->setExpectedException('Exception', 'Error retrieving page: 403');
        HTMLProcessor::fetchHTML($address);
    }
}

